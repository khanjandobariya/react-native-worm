import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      flex: 1,
      padding: scale(20),
      alignItems: 'center'
    },
    titleText: {
      fontFamily: Fonts.bold,
      fontSize: moderateScale(18),
      color: colors.black,
      textAlign: 'center',
      lineHeight: verticalScale(25)
    },
    illustrationImg: {
      width: scale(350),
      height: verticalScale(200),
      marginVertical: verticalScale(40)
    },
    boxContainer: {
      width: scale(300),
      borderWidth: 1,
      borderRadius: moderateScale(10),
      alignItems: 'center',
      padding: scale(20),
      marginBottom: verticalScale(20)
    },
    noteImg: {
      width: scale(120),
      height: scale(120)
    },
    itemTitleContainer: {
      height: verticalScale(50),
      marginVertical: verticalScale(40),
      justifyContent: 'center'
    },
    itemTitle: {
      fontFamily: Fonts.bold,
      fontSize: moderateScale(18),
      color: colors.black,
      textAlign: 'center',
      lineHeight: verticalScale(25)
    }
  })
}
