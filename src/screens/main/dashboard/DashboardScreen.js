import React, {useContext, useRef, useState} from 'react'
import {useTranslation} from 'react-i18next'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import {useNavigation} from '@react-navigation/native'

import {AppContainer, AppScrollView, ConfirmationModal} from '~/components'
import {WormContext} from '~/context'
import {empty, pre_study_eval} from '~/templates/empty'
import {Images, Screen} from '~/utils'
import {useStyle} from './DashboardScreen.styles'

const DashboardScreen = () => {
  const styles = useStyle()
  const navigation = useNavigation()
  const {t} = useTranslation()
  const {setWormSchema} = useContext(WormContext)
  const selectedType = useRef(null)

  const [isConfirmationModal, setConfirmationModal] = useState(false)

  const dummyText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`

  const onPressBlankForm = () => {
    setConfirmationModal(true)
    selectedType.current = 'blank'
  }

  const onPressTemplateForm = () => {
    setConfirmationModal(true)
    selectedType.current = 'template'
  }

  const onClose = () => {
    setConfirmationModal(false)
  }

  const onPressConfirm = () => {
    setConfirmationModal(false)
    if (selectedType.current === 'blank') {
      setWormSchema(empty)
    } else {
      setWormSchema(pre_study_eval)
    }
    navigation.push(Screen.WormScreen)
  }

  return (
    <AppContainer>
      <AppScrollView>
        <View style={styles.container}>
          <Text style={styles.titleText}>{t('WM1')}</Text>
          <Image
            source={Images.illustration1}
            resizeMode={'contain'}
            style={styles.illustrationImg}
          />
          <TouchableOpacity style={styles.boxContainer} onPress={onPressBlankForm}>
            <Image source={Images.notes} resizeMode={'contain'} style={styles.noteImg} />
            <View style={styles.itemTitleContainer}>
              <Text style={styles.itemTitle}>{t('WM2')}</Text>
            </View>
            <Text numberOfLines={4}>{dummyText}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.boxContainer} onPress={onPressTemplateForm}>
            <Image source={Images.form} resizeMode={'contain'} style={styles.noteImg} />
            <View style={styles.itemTitleContainer}>
              <Text style={styles.itemTitle}>{t('WM3')}</Text>
            </View>
            <Text numberOfLines={4}>{dummyText}</Text>
          </TouchableOpacity>
          <ConfirmationModal
            isVisible={isConfirmationModal}
            onClose={onClose}
            onPressConfirm={onPressConfirm}
          />
        </View>
      </AppScrollView>
    </AppContainer>
  )
}

export default DashboardScreen
