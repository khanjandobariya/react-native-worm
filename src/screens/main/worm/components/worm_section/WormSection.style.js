import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts, Opacity} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      width: '100%'
    },
    sectionContainer: {
      flexDirection: 'row',
      height: verticalScale(40),
      marginBottom: -1,
      zIndex: 1
    },
    itemView: {
      backgroundColor: colors.primary,
      borderTopRightRadius: moderateScale(10),
      justifyContent: 'center',
      paddingHorizontal: scale(20)
    },
    borderRadius10: {
      borderTopLeftRadius: moderateScale(10)
    },
    sectionTitle: {
      fontFamily: Fonts.semi_bold,
      fontSize: moderateScale(16),
      color: colors.white
    },
    gragIconContainer: {
      height: '100%',
      aspectRatio: 1,
      backgroundColor: colors.primary,
      borderTopLeftRadius: moderateScale(10),
      justifyContent: 'center',
      alignItems: 'center'
    },
    dotsImg: {
      width: '60%',
      height: '60%',
      tintColor: colors.white
    },
    contentContainer: {
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: colors.greyShadD9,
      backgroundColor: `${colors.white}${Opacity[60]}`,
      borderRadius: moderateScale(10),
      padding: scale(10),
      borderTopLeftRadius: 0,
      alignItems: 'center',
      paddingTop: verticalScale(30)
    },
    border: {
      borderColor: `${colors.black}${Opacity[20]}`,
      borderStyle: 'dashed'
    },
    addBtnContainer: {
      width: scale(150),
      backgroundColor: colors.white,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderStyle: 'dashed',
      borderColor: `${colors.black}${Opacity[20]}`,
      borderRadius: moderateScale(8),
      padding: scale(10),
      marginTop: verticalScale(12)
    },
    btnText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(16),
      color: colors.black
    },
    plusIcon: {
      width: scale(15),
      height: scale(15),
      marginRight: scale(5)
    },
    fieldContainer: {
      width: '100%'
    },
    seperator: {
      height: verticalScale(15)
    }
  })
}
