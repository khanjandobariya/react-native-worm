import React, {useContext, useState} from 'react'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import {NestableDraggableFlatList} from 'react-native-draggable-flatlist'

import {WormContext} from '~/context'
import {Images} from '~/utils'
import FieldAdderModal from '../field_adder_modal/FieldAdderModal'
import RenderField from '../render_field/RenderField'
import {useStyle} from './WormSection.style'

const WormSection = ({item, getIndex, drag}) => {
  const {canEditWormSchema, setSelectedField, onFieldUpdate} = useContext(WormContext)
  const styles = useStyle()
  const [isFieldModal, setFieldModal] = useState(false)

  const onClose = () => {
    setFieldModal(false)
  }

  const onPressField = () => {
    setFieldModal(true)
  }

  const onPressItem = () => {
    setSelectedField(item)
  }

  const onDragEnd = ({data}) => {
    const newSchema = {...item}
    newSchema.fields = data
    onFieldUpdate(newSchema, false)
  }

  const renderItem = (sectionFields) => (
    <RenderField
      field={sectionFields.item}
      sectionIndex={getIndex()}
      index={sectionFields.getIndex()}
      drag={sectionFields.drag}
    />
  )

  const renderSeperator = () => <View style={styles.seperator} />

  const sectionNumber = getIndex() + 1

  return (
    <View style={styles.container}>
      <View style={styles.sectionContainer}>
        {canEditWormSchema && (
          <TouchableOpacity style={styles.gragIconContainer} onPressIn={drag}>
            <Image source={Images.dots} resizeMode={'contain'} style={styles.dotsImg} />
          </TouchableOpacity>
        )}
        <View style={[styles.itemView, !canEditWormSchema && styles.borderRadius10]}>
          <Text
            style={styles.sectionTitle}
          >{`${sectionNumber}. ${item?.wLabel?.label?.text}`}</Text>
        </View>
      </View>
      <TouchableOpacity
        style={[styles.contentContainer, canEditWormSchema && styles.border]}
        onPress={onPressItem}
        disabled={!canEditWormSchema}
      >
        <View style={styles.fieldContainer}>
          <NestableDraggableFlatList
            data={item?.fields}
            extraData={item?.fields}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            onDragEnd={onDragEnd}
            ItemSeparatorComponent={renderSeperator}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps={'handled'}
          />
        </View>
        {canEditWormSchema && (
          <TouchableOpacity style={styles.addBtnContainer} onPress={onPressField}>
            <Image source={Images.plus} resizeMode={'contain'} style={styles.plusIcon} />
            <Text style={styles.btnText}>{'Add Field'}</Text>
          </TouchableOpacity>
        )}
      </TouchableOpacity>
      <FieldAdderModal section={item} isVisible={isFieldModal} onClose={onClose} />
    </View>
  )
}

export default WormSection
