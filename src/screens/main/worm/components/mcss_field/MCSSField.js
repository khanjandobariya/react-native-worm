import React from 'react'
import {useTranslation} from 'react-i18next'
import {Text, TextInput, TouchableOpacity, View} from 'react-native'
import {map} from 'lodash'

import {useStyle} from './MCSSField.styles'

const MCSSField = ({
  field,
  sectionIndex,
  index,
  canEditWormData,
  canEditWormSchema,
  onChangeWormData,
  wormData
}) => {
  const styles = useStyle()
  const {t} = useTranslation()

  const onChangeComment = (comment) => {
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], comment}})
  }

  const onPressSelectChoice = (id) => {
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], value: id}})
  }

  return (
    <View style={styles.flex}>
      <Text style={styles.itemText}>{`${sectionIndex + 1}.${index + 1}   ${
        field?.wLabel?.label?.text
      }`}</Text>
      <View style={styles.rowView}>
        {map(field?.options?.values, (item) => {
          const isSelected = wormData[field.id]?.value === item.id
          return (
            <TouchableOpacity
              style={styles.rowContainer}
              key={item.id}
              onPress={() => onPressSelectChoice(item.id)}
            >
              <View style={[styles.circleView, isSelected && styles.active]} />
              <Text style={styles.optionText}>{item?.wLabel?.label?.text}</Text>
            </TouchableOpacity>
          )
        })}
      </View>
      {!canEditWormSchema && field?.commentsEnabled && (
        <>
          <Text style={styles.commentText}>{t('WM29')}</Text>
          <TextInput
            textAlignVertical={'top'}
            style={styles.commentInput}
            editable={canEditWormData}
            onChangeText={onChangeComment}
            value={wormData[field.id]?.comment || ''}
            multiline
          />
        </>
      )}
    </View>
  )
}

export default MCSSField
