import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    flex: {
      flex: 1
    },
    itemText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black
    },
    commentText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(13),
      color: colors.black,
      marginTop: verticalScale(15)
    },
    commentInput: {
      height: verticalScale(100),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      borderRadius: moderateScale(10),
      backgroundColor: colors.white,
      paddingHorizontal: scale(10),
      marginTop: verticalScale(10),
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black
    },
    rowView: {
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    rowContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: scale(20),
      marginTop: scale(15)
    },
    circleView: {
      width: scale(15),
      height: scale(15),
      borderRadius: moderateScale(200),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      marginRight: scale(10)
    },
    active: {
      borderColor: colors.primary
    },
    optionText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black
    }
  })
}
