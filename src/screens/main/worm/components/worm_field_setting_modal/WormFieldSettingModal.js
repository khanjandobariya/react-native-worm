import React, {useContext} from 'react'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import Modal from 'react-native-modal'

import {AppContainer, AppScrollView} from '~/components'
import {WormContext} from '~/context'
import {Images} from '~/utils'
import FieldProperties from '../field_properties/FieldProperties'
import {useStyle} from './WormFieldSettingModal.styles'

const WormFieldSettingModal = () => {
  const styles = useStyle()
  const {onFieldDelete, selectedField, setSelectedField} = useContext(WormContext)

  const onClose = () => {
    setSelectedField(null)
  }

  const onDelete = () => onFieldDelete(selectedField)

  return (
    <Modal
      isVisible={!!selectedField}
      animationIn={'fadeInRight'}
      animationOut={'fadeOutRight'}
      style={styles.container}
      onBackdropPress={onClose}
    >
      <View style={styles.contentContainer}>
        <AppContainer>
          <TouchableOpacity style={styles.closeBtn} onPress={onClose}>
            <Image source={Images.close} resizeMode={'contain'} style={styles.closeBtnIcon} />
          </TouchableOpacity>
          <AppScrollView>
            <View style={styles.mainView}>
              <View style={styles.allBtnView}>
                <View style={styles.rowContainer}>
                  <TouchableOpacity style={styles.exportBtn}>
                    <Image source={Images.share} resizeMode={'contain'} style={styles.shareIcon} />
                    <Text style={styles.exportBtnText}>{'Export'}</Text>
                  </TouchableOpacity>
                  {selectedField?.type !== 'W' && (
                    <TouchableOpacity style={styles.deleteBtn} onPress={onDelete}>
                      <Image
                        source={Images.delete}
                        resizeMode={'contain'}
                        style={styles.shareIcon}
                      />
                      <Text style={styles.saveBtnText}>{'Delete'}</Text>
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity style={styles.saveBtn}>
                    <Image source={Images.save} resizeMode={'contain'} style={styles.shareIcon} />
                    <Text style={styles.saveBtnText}>{'Save'}</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <FieldProperties />
            </View>
          </AppScrollView>
        </AppContainer>
      </View>
    </Modal>
  )
}

export default WormFieldSettingModal
