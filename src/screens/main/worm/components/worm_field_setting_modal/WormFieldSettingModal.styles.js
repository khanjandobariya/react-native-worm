import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      alignItems: 'flex-end',
      margin: 0
    },
    contentContainer: {
      width: '95%',
      height: '100%',
      backgroundColor: colors.white
    },
    closeBtn: {
      width: scale(45),
      height: scale(45),
      aspectRatio: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: scale(20)
    },
    closeBtnIcon: {
      width: '50%',
      height: '50%',
      tintColor: colors.greyShadD9
    },
    mainView: {
      alignItems: 'center'
    },
    allBtnView: {
      alignItems: 'center',
      marginTop: verticalScale(20)
    },
    rowContainer: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      gap: 10
    },
    exportBtn: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: moderateScale(6),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      paddingHorizontal: scale(15),
      paddingVertical: scale(5)
    },
    shareIcon: {
      width: scale(24),
      height: scale(24),
      marginRight: scale(10)
    },
    exportBtnText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(16),
      color: colors.black
    },
    deleteBtn: {
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: moderateScale(6),
      paddingHorizontal: scale(15),
      paddingVertical: scale(5),
      backgroundColor: colors.redShadFF
    },
    saveBtn: {
      width: scale(100),
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: moderateScale(6),
      paddingHorizontal: scale(15),
      paddingVertical: scale(5),
      backgroundColor: colors.primary
    },
    saveBtnText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(16),
      color: colors.white
    },
    textInputView: {
      marginTop: verticalScale(50)
    }
  })
}
