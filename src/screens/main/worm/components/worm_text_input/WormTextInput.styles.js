import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      width: scale(350)
    },
    labelText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black
    },
    textInput: {
      height: verticalScale(40),
      borderRadius: moderateScale(6),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      paddingHorizontal: scale(15),
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black,
      marginTop: verticalScale(5),
      paddingVertical: 0
    }
  })
}
