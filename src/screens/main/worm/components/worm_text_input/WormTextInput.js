import React from 'react'
import {Text, TextInput, View} from 'react-native'

import {useStyle} from './WormTextInput.styles'

const WormTextInput = ({containerStyle, label, defaultValue, onChangeText}) => {
  const styles = useStyle()

  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.labelText}>{label}</Text>
      <TextInput defaultValue={defaultValue} onChangeText={onChangeText} style={styles.textInput} />
    </View>
  )
}

export default WormTextInput
