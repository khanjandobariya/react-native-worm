import React, {useContext} from 'react'
import {useTranslation} from 'react-i18next'
import {Text, View} from 'react-native'
import CheckBox from 'react-native-check-box'

import {WormContext} from '~/context'
import {useColor} from '~/hooks'
import WormTextInput from '../../worm_text_input/WormTextInput'
import {useStyle} from './DateFieldProperties.styles'

const DateFieldProperties = () => {
  const styles = useStyle()
  const colors = useColor()
  const {t} = useTranslation()
  const {onFieldUpdate, selectedField, setSelectedField} = useContext(WormContext)

  const onChangeLabel = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({
      ...updatedField,
      wLabel: {...updatedField.wLabel, label: {...updatedField.wLabel.label, text: value}}
    })
  }

  const onChangePlaceHolder = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({...updatedField, phold: value})
  }

  const onChangeDesc = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({...updatedField, desc: value})
  }

  const onChangeCheckBox = () => {
    let updatedField = JSON.parse(JSON.stringify(selectedField))
    updatedField = {...updatedField, commentsEnabled: !updatedField?.commentsEnabled}
    onFieldUpdate(updatedField)
    setSelectedField(updatedField)
  }

  return (
    <View style={styles.container}>
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM22')}
        defaultValue={selectedField?.wLabel?.label?.text}
        onChangeText={onChangeLabel}
      />
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM23')}
        defaultValue={selectedField?.phold}
        onChangeText={onChangePlaceHolder}
      />
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM24')}
        defaultValue={selectedField?.desc}
        onChangeText={onChangeDesc}
      />
      <View style={styles.checkBoxContainer}>
        <CheckBox
          style={styles.checkBox}
          onClick={onChangeCheckBox}
          isChecked={selectedField?.commentsEnabled}
          checkBoxColor={colors.primary}
          checkedCheckBoxColor={colors.primary}
          uncheckedCheckBoxColor={colors.greyShadD9}
        />
        <Text style={styles.commentText}>{t('WM25')}</Text>
      </View>
    </View>
  )
}

export default DateFieldProperties
