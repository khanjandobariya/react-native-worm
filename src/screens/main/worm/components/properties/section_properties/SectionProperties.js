import React, {useContext} from 'react'
import {View} from 'react-native'

import {WormContext} from '~/context'
import WormTextInput from '../../worm_text_input/WormTextInput'
import {useStyle} from './SectionProperties.styles'

const SectionProperties = () => {
  const styles = useStyle()
  const {onFieldUpdate, selectedField} = useContext(WormContext)

  const onChangeHeadingText = (value) => {
    const updatedSection = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({
      ...updatedSection,
      wLabel: {...updatedSection.wLabel, label: {...updatedSection.wLabel.label, text: value}}
    })
  }

  const onChangeDescText = (value) => {
    const updatedSection = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({...updatedSection, desc: value})
  }

  return (
    <View style={styles.container}>
      <WormTextInput
        containerStyle={styles.textInputView}
        label={'Edit Section Heading'}
        defaultValue={selectedField.wLabel.label.text}
        onChangeText={onChangeHeadingText}
      />
      <WormTextInput
        containerStyle={styles.textInputView}
        label={'Edit Description'}
        defaultValue={selectedField.desc}
        onChangeText={onChangeDescText}
      />
    </View>
  )
}

export default SectionProperties
