import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      marginTop: verticalScale(30)
    },
    textInputView: {
      marginTop: verticalScale(20)
    },
    checkBoxContainer: {
      marginTop: verticalScale(20),
      flexDirection: 'row',
      alignItems: 'center'
    },
    checkBox: {
      marginRight: scale(10)
    },
    commentText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black
    }
  })
}
