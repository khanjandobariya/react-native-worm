import {StyleSheet} from 'react-native'

import {useResponsive} from '~/hooks'

export const useStyle = () => {
  const {verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      marginTop: verticalScale(30)
    },
    textInputView: {
      marginTop: verticalScale(20)
    }
  })
}
