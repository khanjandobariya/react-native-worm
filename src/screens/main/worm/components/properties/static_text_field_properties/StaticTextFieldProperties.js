import React, {useContext} from 'react'
import {useTranslation} from 'react-i18next'
import {View} from 'react-native'

import {WormContext} from '~/context'
import WormTextInput from '../../worm_text_input/WormTextInput'
import {useStyle} from './StaticTextFieldProperties.styles'

const StaticTextFieldProperties = () => {
  const styles = useStyle()
  const {t} = useTranslation()
  const {onFieldUpdate, selectedField} = useContext(WormContext)

  const onChangeText = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({
      ...updatedField,
      wLabel: {...updatedField.wLabel, label: {...updatedField.wLabel.label, text: value}}
    })
  }

  return (
    <View style={styles.container}>
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM28')}
        defaultValue={selectedField?.wLabel?.label?.text}
        onChangeText={onChangeText}
      />
    </View>
  )
}

export default StaticTextFieldProperties
