import React, {useContext} from 'react'
import {useTranslation} from 'react-i18next'
import {Text, TouchableOpacity, View} from 'react-native'
import {map} from 'lodash'

import {AppButton} from '~/components'
import {WormContext} from '~/context'
import WormTextInput from '../../worm_text_input/WormTextInput'
import {useStyle} from './WormFieldProperties.styles'

const WormFieldProperties = () => {
  const COLORS = [
    '#fafafa',
    '#fde4cf',
    '#ffcfd2',
    '#f1c0e8',
    '#cfbaf0',
    '#a3c3f3',
    '#90dbf4',
    '#8eecf5',
    '#98f5e1'
  ]
  const styles = useStyle()
  const {t} = useTranslation()
  const {onWormSchemaChange, selectedField, onNewSection} = useContext(WormContext)

  const onChangeText = (value) => {
    const newSchema = {...selectedField}
    newSchema.wLabel.label.text = value
    onWormSchemaChange(newSchema)
  }

  const onPressColor = (color) => {
    const newSchema = {...selectedField}
    newSchema.ui.style = {...newSchema.ui.style, backgroundColor: color}
    onWormSchemaChange(newSchema)
  }

  return (
    <View>
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM19')}
        defaultValue={selectedField.wLabel.label.text}
        onChangeText={onChangeText}
      />
      <View style={styles.colorContainer}>
        <Text style={styles.colorText}>{t('WM20')}</Text>
        <View style={styles.rowView}>
          {map(COLORS, (color, index) => (
            <TouchableOpacity
              key={`color-${index}`}
              style={[styles.boxView, {backgroundColor: color}]}
              onPress={() => onPressColor(color)}
            />
          ))}
        </View>
      </View>
      <AppButton
        textStyle={styles.btnText}
        containerStyle={styles.btnView}
        label={t('WM21')}
        onPress={onNewSection}
      />
    </View>
  )
}

export default WormFieldProperties
