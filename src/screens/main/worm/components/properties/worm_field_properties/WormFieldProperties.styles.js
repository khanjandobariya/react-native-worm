import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    textInputView: {
      marginTop: verticalScale(50)
    },
    colorContainer: {
      width: scale(350),
      marginTop: verticalScale(30)
    },
    colorText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black
    },
    rowView: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      gap: scale(11),
      marginTop: verticalScale(5)
    },
    boxView: {
      width: scale(40),
      height: scale(40),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      borderRadius: moderateScale(6)
    },
    btnView: {
      width: scale(350),
      marginTop: verticalScale(30),
      backgroundColor: colors.primary
    },
    btnText: {
      color: colors.white
    }
  })
}
