import React, {useContext} from 'react'
import {useTranslation} from 'react-i18next'
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native'
import CheckBox from 'react-native-check-box'
import {find, map} from 'lodash'
import shortid from 'shortid'

import {AppButton} from '~/components'
import {WormContext} from '~/context'
import {useColor} from '~/hooks'
import {Images} from '~/utils'
import {newChoiceValue} from '~/utils/new-choice-value'
import WormTextInput from '../../worm_text_input/WormTextInput'
import {useStyle} from './MCSSFieldProperties.styles'

const MCSSFieldProperties = () => {
  const styles = useStyle()
  const colors = useColor()
  const {t} = useTranslation()
  const {onFieldUpdate, selectedField, setSelectedField} = useContext(WormContext)

  const onChangeLabel = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({
      ...updatedField,
      wLabel: {...updatedField.wLabel, label: {...updatedField.wLabel.label, text: value}}
    })
  }

  const onChangeDesc = (value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    onFieldUpdate({...updatedField, desc: value})
  }

  const onChangeCheckBox = () => {
    let updatedField = JSON.parse(JSON.stringify(selectedField))
    updatedField = {...updatedField, commentsEnabled: !updatedField?.commentsEnabled}
    onFieldUpdate(updatedField)
    setSelectedField(updatedField)
  }

  const onPressNewChoice = () => {
    const newChoiceVal = {...newChoiceValue}
    newChoiceVal.id = shortid.generate()
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    updatedField.options.values.push(newChoiceVal)
    onFieldUpdate(updatedField)
    setSelectedField(updatedField)
  }

  const onChangeChoiceText = (item, value) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    const targetValue = find(updatedField.options.values, {id: item.id})
    if (!targetValue) {
      return
    }
    targetValue.wLabel.label.text = value
    onFieldUpdate(updatedField)
    setSelectedField(updatedField)
  }

  const onPressDeleteChoice = (index) => {
    const updatedField = JSON.parse(JSON.stringify(selectedField))
    updatedField.options.values.splice(index, 1)
    onFieldUpdate(updatedField)
    setSelectedField(updatedField)
  }

  return (
    <View style={styles.container}>
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM22')}
        defaultValue={selectedField?.wLabel?.label?.text}
        onChangeText={onChangeLabel}
      />
      <WormTextInput
        containerStyle={styles.textInputView}
        label={t('WM24')}
        defaultValue={selectedField?.desc}
        onChangeText={onChangeDesc}
      />
      <View style={styles.checkBoxContainer}>
        <CheckBox
          style={styles.checkBox}
          onClick={onChangeCheckBox}
          isChecked={selectedField?.commentsEnabled}
          checkBoxColor={colors.primary}
          checkedCheckBoxColor={colors.primary}
          uncheckedCheckBoxColor={colors.greyShadD9}
        />
        <Text style={styles.commentText}>{t('WM25')}</Text>
      </View>
      <View style={styles.choiceContainer}>
        <Text style={styles.choiceText}>{t('WM31')}</Text>
        {map(selectedField?.options?.values, (item) => {
          return (
            <View style={styles.rowView} key={`option-${item.id}`}>
              <TextInput
                value={item?.wLabel?.label?.text}
                style={styles.inputText}
                onChangeText={(value) => onChangeChoiceText(item, value)}
              />
              <TouchableOpacity style={styles.binContainer} onPress={onPressDeleteChoice}>
                <Image source={Images.bin} resizeMode={'contain'} style={styles.binIcon} />
              </TouchableOpacity>
            </View>
          )
        })}
      </View>
      <AppButton
        textStyle={styles.btnText}
        containerStyle={styles.btnView}
        label={t('WM30')}
        onPress={onPressNewChoice}
      />
    </View>
  )
}

export default MCSSFieldProperties
