import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      marginTop: verticalScale(30)
    },
    textInputView: {
      marginTop: verticalScale(20)
    },
    checkBoxContainer: {
      marginTop: verticalScale(20),
      flexDirection: 'row',
      alignItems: 'center'
    },
    checkBox: {
      marginRight: scale(10)
    },
    commentText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black
    },
    btnView: {
      width: scale(350),
      marginTop: verticalScale(30),
      backgroundColor: colors.primary
    },
    btnText: {
      color: colors.white
    },
    choiceContainer: {
      marginTop: verticalScale(20)
    },
    choiceText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black
    },
    rowView: {
      flexDirection: 'row',
      marginTop: verticalScale(15)
    },
    inputText: {
      flex: 1,
      height: verticalScale(40),
      borderRadius: moderateScale(6),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      paddingHorizontal: scale(15),
      fontFamily: Fonts.regular,
      fontSize: moderateScale(16),
      color: colors.black,
      paddingVertical: 0,
      marginRight: scale(10)
    },
    binContainer: {
      width: verticalScale(40),
      height: verticalScale(40),
      backgroundColor: colors.redShadFF,
      borderRadius: moderateScale(6),
      justifyContent: 'center',
      alignItems: 'center'
    },
    binIcon: {
      width: '50%',
      height: '50%'
    }
  })
}
