import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Opacity} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale} = useResponsive()

  return StyleSheet.create({
    itemContainer: {
      borderWidth: 1,
      borderStyle: 'dashed',
      borderColor: colors.transparent,
      borderRadius: moderateScale(10),
      padding: scale(10),
      flexDirection: 'row',
      alignItems: 'center'
    },
    border: {
      borderColor: `${colors.black}${Opacity[20]}`
    },
    dragItemIcon: {
      height: scale(35),
      aspectRatio: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    dotsItemImg: {
      width: '60%',
      height: '60%',
      tintColor: colors.black
    }
  })
}
