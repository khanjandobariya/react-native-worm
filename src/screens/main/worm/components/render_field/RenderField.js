import React, {useContext} from 'react'
import {Image, TouchableOpacity} from 'react-native'

import {WormContext} from '~/context'
import {Images} from '~/utils'
import DateField from '../date_field/DateField'
import MCSSField from '../mcss_field/MCSSField'
import StaticTextField from '../static_text_field/StaticTextField'
import TextField from '../text_field/TextField'
import {useStyle} from './RenderField.styles'

const RenderField = ({field, sectionIndex, index, drag}) => {
  const styles = useStyle()
  const {wormData, setSelectedField, canEditWormSchema, canEditWormData, onChangeWormData} =
    useContext(WormContext)

  const onPressItem = () => {
    setSelectedField(field)
  }

  const renderFieldComponent = () => {
    if (field.type === 'S') {
      return (
        <TextField
          wormData={wormData}
          canEditWormData={canEditWormData}
          canEditWormSchema={canEditWormSchema}
          field={field}
          sectionIndex={sectionIndex}
          index={index}
          onChangeWormData={onChangeWormData}
        />
      )
    }
    if (field.type === 'ST') {
      return <StaticTextField field={field} sectionIndex={sectionIndex} index={index} />
    }
    if (field.type === 'DT') {
      return (
        <DateField
          wormData={wormData}
          canEditWormData={canEditWormData}
          canEditWormSchema={canEditWormSchema}
          field={field}
          sectionIndex={sectionIndex}
          index={index}
          onChangeWormData={onChangeWormData}
        />
      )
    }
    if (field.type === 'EN') {
      return (
        <MCSSField
          wormData={wormData}
          canEditWormData={canEditWormData}
          canEditWormSchema={canEditWormSchema}
          field={field}
          sectionIndex={sectionIndex}
          index={index}
          onChangeWormData={onChangeWormData}
        />
      )
    }
  }

  return (
    <TouchableOpacity
      style={[styles.itemContainer, canEditWormSchema && styles.border]}
      onPress={onPressItem}
      disabled={!canEditWormSchema}
    >
      {renderFieldComponent()}
      {canEditWormSchema && (
        <TouchableOpacity style={styles.dragItemIcon} onPressIn={drag}>
          <Image source={Images.dots} resizeMode={'contain'} style={styles.dotsItemImg} />
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  )
}

export default RenderField
