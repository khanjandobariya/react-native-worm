import React, {useContext, useState} from 'react'
import {useTranslation} from 'react-i18next'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import Modal from 'react-native-modal'
import {map} from 'lodash'
import shortid from 'shortid'

import {AppButton} from '~/components'
import {WormContext} from '~/context'
import {Images} from '~/utils'
import {newYesNoMcssField} from '~/utils/new-custom-fields'
import {
  newDateField,
  newSingleValueEnumField,
  newStaticTextField,
  newTextField
} from '~/utils/new-field'
import {useStyle} from './FieldAdderModal.styles'

const FieldAdderModal = ({section, isVisible, onClose}) => {
  const styles = useStyle()
  const {t} = useTranslation()
  const {onFieldUpdate} = useContext(WormContext)

  const ROUTES = [t('WM8'), t('WM9')]
  const [index, setIndex] = useState(0)

  const onPressBtn = (type) => {
    let newField = null
    if (type === 1) {
      newField = {...newTextField}
    } else if (type === 2) {
      newField = {...newDateField}
    } else if (type === 3) {
      newField = {...newSingleValueEnumField}
    } else if (type === 4) {
      newField = {...newStaticTextField}
    } else if (type === 5) {
      newField = {...newYesNoMcssField}
    }
    newField.id = shortid.generate()
    const updatedSection = JSON.parse(JSON.stringify(section))
    updatedSection.fields.push(newField)
    onFieldUpdate(updatedSection, false)
  }

  const onPressTab = (index) => {
    setIndex(index)
  }

  const renderTabView = () => {
    if (index === 0) {
      return (
        <View style={styles.tabContainer}>
          <View style={styles.fieldContainer}>
            <AppButton
              containerStyle={styles.btnView}
              icon={Images.clip}
              label={t('WM10')}
              onPress={() => onPressBtn(1)}
            />
            <AppButton
              containerStyle={styles.btnView}
              icon={Images.calender}
              label={t('WM11')}
              onPress={() => onPressBtn(2)}
            />
            <AppButton
              containerStyle={styles.btnView}
              icon={Images.multiple}
              label={t('WM12')}
              onPress={() => onPressBtn(3)}
            />
            <AppButton
              containerStyle={styles.btnView}
              icon={Images.msg}
              label={t('WM13')}
              onPress={() => onPressBtn(4)}
            />
          </View>
          <AppButton containerStyle={styles.doneBtn} label={t('WM14')} onPress={onClose} />
        </View>
      )
    }
    if (index === 1) {
      return (
        <View style={styles.tabContainer}>
          <View style={styles.fieldContainer}>
            <AppButton
              containerStyle={styles.btnView}
              icon={Images.options}
              label={t('WM15')}
              onPress={() => onPressBtn(5)}
            />
          </View>
          <AppButton containerStyle={styles.doneBtn} label={t('WM14')} onPress={onClose} />
        </View>
      )
    }
  }

  const renderTabBar = () => {
    return (
      <View style={styles.tabView}>
        {map(ROUTES, (title, i) => {
          const isActive = index === i
          return (
            <TouchableOpacity
              style={[styles.tab, isActive && styles.activeTab]}
              key={`tab-${i}`}
              onPress={() => onPressTab(i)}
            >
              <Text style={[styles.tabText, isActive && styles.activeTabText]}>{title}</Text>
            </TouchableOpacity>
          )
        })}
      </View>
    )
  }

  return (
    <Modal
      isVisible={isVisible}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={styles.container}
      onBackdropPress={onClose}
    >
      <View style={styles.contentContainer}>
        <View style={styles.rowView}>
          <View style={styles.titleRow}>
            <Image source={Images.plus} resizeMode={'contain'} style={styles.plusIcon} />
            <Text style={styles.title}>{t('WM16')}</Text>
          </View>
          <TouchableOpacity style={styles.closeBtn} onPress={onClose}>
            <Image source={Images.close} resizeMode={'contain'} style={styles.closeBtnIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.boxView}>
          {renderTabBar()}
          {renderTabView()}
        </View>
      </View>
    </Modal>
  )
}

export default FieldAdderModal
