import {StyleSheet} from 'react-native'

import {useColor} from '~/hooks'
import {Fonts} from '~/theme'
import {moderateScale, scale, verticalScale} from '~/utils/Responsive'

export const useStyle = () => {
  const colors = useColor()

  return StyleSheet.create({
    container: {
      margin: 0,
      flex: 1,
      marginHorizontal: scale(20)
    },
    contentContainer: {
      width: '100%',
      backgroundColor: colors.white,
      padding: scale(20),
      borderRadius: moderateScale(8)
    },
    rowView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    titleRow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    title: {
      fontSize: scale(16),
      fontFamily: Fonts.semi_bold,
      color: colors.black
    },
    boxView: {
      width: '100%',
      marginTop: verticalScale(20)
    },
    closeBtn: {
      width: scale(18),
      height: scale(18),
      aspectRatio: 1,
      justifyContent: 'center',
      alignItems: 'flex-end'
    },
    closeBtnIcon: {
      width: '100%',
      height: '100%',
      tintColor: colors.secondary
    },
    plusIcon: {
      width: scale(16),
      height: scale(16),
      marginRight: scale(5)
    },
    tabView: {
      width: '100%',
      alignSelf: 'center',
      height: verticalScale(28),
      alignItems: 'center',
      flexDirection: 'row',
      borderBottomColor: colors.greyShadD9,
      borderBottomWidth: 0.5
    },
    tab: {
      height: '100%',
      borderBottomColor: colors.primary,
      borderBottomWidth: 0,
      marginRight: scale(20)
    },
    activeTab: {
      borderBottomColor: colors.primary,
      borderBottomWidth: 2
    },
    activeTabText: {
      color: colors.primary
    },
    tabText: {
      fontFamily: Fonts.medium,
      color: colors.black,
      fontSize: moderateScale(14)
    },
    fieldContainer: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      borderRadius: moderateScale(8),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      padding: scale(12),
      gap: scale(12)
    },
    btnView: {
      height: verticalScale(35),
      paddingHorizontal: scale(15)
    },
    tabContainer: {
      marginTop: verticalScale(15)
    },
    doneBtn: {
      marginTop: verticalScale(15),
      width: scale(75),
      height: verticalScale(35),
      alignSelf: 'flex-end'
    }
  })
}
