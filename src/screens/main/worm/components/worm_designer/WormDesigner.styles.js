import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale} = useResponsive()

  return StyleSheet.create({
    container: {
      flex: 1
    },
    rowView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: scale(20)
    },
    btnContainer: {
      width: scale(40),
      height: scale(40),
      backgroundColor: colors.white,
      borderRadius: moderateScale(6),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      justifyContent: 'center',
      alignItems: 'center',
      marginLeft: scale(15)
    },
    menuIcon: {
      width: '45%',
      height: '45%'
    },
    rowContainer: {
      flexDirection: 'row'
    }
  })
}
