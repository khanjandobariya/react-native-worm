import React, {useContext, useState} from 'react'
import {Image, TouchableOpacity, View} from 'react-native'

import {SettingModal} from '~/components'
import {WormContext} from '~/context'
import {Images} from '~/utils'
import WormFieldSettingModal from '../worm_field_setting_modal/WormFieldSettingModal'
import WormRenderer from '../worm_renderer/WormRenderer'
import {useStyle} from './WormDesigner.styles'

const WormDesigner = () => {
  const styles = useStyle()
  const {canEditWormSchema, setSelectedField, wormSchema} = useContext(WormContext)

  const [isSettingModal, setSettingModal] = useState(false)

  const onPressSetting = () => {
    setSelectedField(wormSchema)
  }

  const onPressMore = () => {
    setSettingModal(true)
  }

  const onRequestClose = () => {
    setSettingModal(false)
  }

  return (
    <View style={styles.container}>
      <View style={styles.rowView}>
        <TouchableOpacity style={styles.btnContainer}>
          <Image source={Images.menu} resizeMode={'contain'} style={styles.menuIcon} />
        </TouchableOpacity>
        <View style={styles.rowContainer}>
          {canEditWormSchema && (
            <TouchableOpacity style={styles.btnContainer} onPress={onPressSetting}>
              <Image source={Images.setting} resizeMode={'contain'} style={styles.menuIcon} />
            </TouchableOpacity>
          )}
          <TouchableOpacity style={styles.btnContainer} onPress={onPressMore}>
            <Image source={Images.more} resizeMode={'contain'} style={styles.menuIcon} />
          </TouchableOpacity>
        </View>
      </View>
      <WormRenderer />
      <WormFieldSettingModal />
      <SettingModal isVisible={isSettingModal} onRequestClose={onRequestClose} />
    </View>
  )
}

export default WormDesigner
