import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    detailItem: {
      flex: 1
    },
    itemText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black,
      lineHeight: verticalScale(20)
    }
  })
}
