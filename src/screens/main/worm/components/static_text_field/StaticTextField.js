import React from 'react'
import {Text, View} from 'react-native'

import {useStyle} from './StaticTextField.styles'

const StaticTextField = ({field, sectionIndex, index}) => {
  const styles = useStyle()
  return (
    <View style={styles.detailItem}>
      <Text style={styles.itemText}>{`${sectionIndex + 1}.${index + 1}   ${
        field?.wLabel?.label?.text
      }`}</Text>
    </View>
  )
}

export default StaticTextField
