import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    flex: {
      flex: 1
    },
    itemText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black
    },
    rowView: {
      flexDirection: 'row',
      alignItems: 'center',
      height: verticalScale(40),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      borderRadius: moderateScale(10),
      backgroundColor: colors.white,
      marginTop: verticalScale(10)
    },
    textInput: {
      flex: 1,
      paddingHorizontal: scale(10),
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black,
      paddingVertical: 0
    },
    icon: {
      width: scale(20),
      height: scale(20),
      tintColor: colors.greyShadD9,
      marginRight: scale(10)
    },
    commentText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(13),
      color: colors.black,
      marginTop: verticalScale(10)
    },
    commentInput: {
      height: verticalScale(100),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      borderRadius: moderateScale(10),
      backgroundColor: colors.white,
      paddingHorizontal: scale(10),
      marginTop: verticalScale(10),
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black
    }
  })
}
