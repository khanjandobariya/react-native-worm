import React, {useState} from 'react'
import {useTranslation} from 'react-i18next'
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native'
import DateTimePickerModal from 'react-native-modal-datetime-picker'
import moment from 'moment'

import {Images} from '~/utils'
import {useStyle} from './DateField.styles'

const DateField = ({
  field,
  sectionIndex,
  index,
  canEditWormData,
  canEditWormSchema,
  onChangeWormData,
  wormData
}) => {
  const styles = useStyle()
  const {t} = useTranslation()
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false)

  const onPressDate = () => {
    setDatePickerVisibility(true)
  }

  const onCancel = () => {
    setDatePickerVisibility(false)
  }

  const onSelectDate = (date) => {
    setDatePickerVisibility(false)
    const value = moment(date).format('MM-DD-YYYY')
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], value}})
  }

  const onChangeComment = (comment) => {
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], comment}})
  }

  return (
    <View style={styles.flex}>
      <Text style={styles.itemText}>{`${sectionIndex + 1}.${index + 1}   ${
        field?.wLabel?.label?.text
      }`}</Text>
      <TouchableOpacity
        style={styles.rowView}
        disabled={!canEditWormData && canEditWormSchema}
        onPress={onPressDate}
      >
        <View pointerEvents={'none'} style={styles.flex}>
          <TextInput
            placeholder={field?.phold || ''}
            style={styles.textInput}
            editable={false}
            value={wormData[field.id]?.value || ''}
          />
        </View>
        <Image source={Images.newcalender} resizeMode={'contain'} style={styles.icon} />
      </TouchableOpacity>
      {!canEditWormSchema && field?.commentsEnabled && (
        <>
          <Text style={styles.commentText}>{t('WM29')}</Text>
          <TextInput
            textAlignVertical={'top'}
            style={styles.commentInput}
            editable={canEditWormData}
            onChangeText={onChangeComment}
            value={wormData[field.id]?.comment || ''}
            multiline
          />
        </>
      )}
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={'date'}
        onConfirm={onSelectDate}
        onCancel={onCancel}
      />
    </View>
  )
}

export default DateField
