import React, {useContext} from 'react'

import {WormContext} from '~/context'
import DateFieldProperties from '../properties/date_field_properties/DateFieldProperties'
import MCSSFieldProperties from '../properties/mcss_field_properties/MCSSFieldProperties'
import SectionProperties from '../properties/section_properties/SectionProperties'
import StaticTextFieldProperties from '../properties/static_text_field_properties/StaticTextFieldProperties'
import TextFieldProperties from '../properties/text_field_properties/TextFieldProperties'
import WormFieldProperties from '../properties/worm_field_properties/WormFieldProperties'

const FieldProperties = () => {
  const {selectedField} = useContext(WormContext)

  if (selectedField?.type === 'W') {
    return <WormFieldProperties key={selectedField?.id} />
  }

  if (selectedField?.type === 'SEC') {
    return <SectionProperties key={selectedField?.id} />
  }

  if (selectedField?.type === 'S') {
    return <TextFieldProperties key={selectedField?.id} />
  }

  if (selectedField?.type === 'ST') {
    return <StaticTextFieldProperties key={selectedField?.id} />
  }

  if (selectedField?.type === 'DT') {
    return <DateFieldProperties key={selectedField?.id} />
  }

  if (selectedField?.type === 'EN') {
    return <MCSSFieldProperties key={selectedField?.id} />
  }

  return null
}

export default FieldProperties
