import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts, Opacity} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      // flex: 1,
      padding: scale(20),
      alignItems: 'center'
    },
    flex: {
      flex: 1
    },
    noteImg: {
      width: scale(120),
      height: scale(120)
    },
    titleText: {
      fontFamily: Fonts.bold,
      fontSize: moderateScale(24),
      color: colors.black,
      textAlign: 'center',
      marginVertical: verticalScale(30)
    },
    emptyContainer: {
      marginVertical: verticalScale(50),
      alignItems: 'center'
    },
    emptyIcon: {
      width: scale(250),
      height: verticalScale(100)
    },
    emptyText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(14),
      color: colors.black,
      marginTop: verticalScale(10)
    },
    addBtnContainer: {
      backgroundColor: colors.white,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 1,
      borderStyle: 'dashed',
      borderColor: `${colors.black}${Opacity[20]}`,
      borderRadius: moderateScale(8),
      padding: scale(10),
      marginTop: verticalScale(30)
    },
    btnText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(16),
      color: colors.black
    },
    plusIcon: {
      width: scale(15),
      height: scale(15),
      marginRight: scale(5)
    },
    fieldContainer: {
      width: '100%'
    },
    seperator: {
      height: verticalScale(20)
    }
  })
}
