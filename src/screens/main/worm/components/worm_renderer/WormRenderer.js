import React, {useContext} from 'react'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import {NestableDraggableFlatList, NestableScrollContainer} from 'react-native-draggable-flatlist'

import {AppScrollView} from '~/components'
import {WormContext} from '~/context'
import {Images} from '~/utils'
import WormSection from '../worm_section/WormSection'
import {useStyle} from './WormRenderer.styles'

const WormRenderer = () => {
  const styles = useStyle()
  const {canEditWormSchema, wormSchema, onNewSection, onFieldUpdate} = useContext(WormContext)

  const onDragEnd = ({data}) => {
    const newSchema = {...wormSchema}
    newSchema.fields = data
    onFieldUpdate(newSchema, false)
  }

  const renderItem = (item) => <WormSection {...item} />

  const renderSeperator = () => <View style={styles.seperator} />

  const ParentScroll = canEditWormSchema ? View : AppScrollView

  return (
    <ParentScroll showsVerticalScrollIndicator={false} keyboardShouldPersistTaps={'handled'}>
      <NestableScrollContainer
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'handled'}
        scrollEnabled={canEditWormSchema}
      >
        <View style={styles.container}>
          {wormSchema?.wLabel?.ui?.imgSrc && (
            <Image
              source={wormSchema?.wLabel?.ui?.imgSrc}
              resizeMode={'contain'}
              style={styles.noteImg}
            />
          )}
          <Text style={[styles.titleText, wormSchema?.wLabel?.ui?.style]}>
            {wormSchema?.wLabel?.label?.text}
          </Text>
          {wormSchema?.fields?.length === 0 && (
            <View style={styles.emptyContainer}>
              <Image source={Images.empty} resizeMode={'contain'} style={styles.emptyIcon} />
              <Text style={styles.emptyText}>{'Empty Form'}</Text>
            </View>
          )}

          <View style={styles.fieldContainer}>
            <NestableDraggableFlatList
              data={wormSchema?.fields}
              extraData={wormSchema?.fields}
              renderItem={renderItem}
              keyExtractor={(item) => item.id}
              onDragEnd={onDragEnd}
              ItemSeparatorComponent={renderSeperator}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps={'handled'}
            />
          </View>
          {canEditWormSchema && (
            <TouchableOpacity style={styles.addBtnContainer} onPress={onNewSection}>
              <Image source={Images.plus} resizeMode={'contain'} style={styles.plusIcon} />
              <Text style={styles.btnText}>{'Add Section'}</Text>
            </TouchableOpacity>
          )}
        </View>
      </NestableScrollContainer>
    </ParentScroll>
  )
}

export default WormRenderer
