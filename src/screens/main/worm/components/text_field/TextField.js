import React from 'react'
import {useTranslation} from 'react-i18next'
import {Text, TextInput, View} from 'react-native'

import {useStyle} from './TextField.styles'

const TextField = ({
  field,
  sectionIndex,
  index,
  canEditWormData,
  canEditWormSchema,
  onChangeWormData,
  wormData
}) => {
  const styles = useStyle()
  const {t} = useTranslation()

  const onChangeText = (value) => {
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], value}})
  }

  const onChangeComment = (comment) => {
    onChangeWormData({...wormData, [field.id]: {...wormData[field.id], comment}})
  }

  return (
    <View style={styles.detailItem}>
      <Text style={styles.itemText}>{`${sectionIndex + 1}.${index + 1}   ${
        field?.wLabel?.label?.text
      }`}</Text>
      <TextInput
        placeholder={field?.phold || ''}
        style={styles.textInput}
        editable={canEditWormData}
        onChangeText={onChangeText}
        value={wormData[field.id]?.value || ''}
      />
      {!canEditWormSchema && field?.commentsEnabled && (
        <>
          <Text style={styles.commentText}>{t('WM29')}</Text>
          <TextInput
            textAlignVertical={'top'}
            style={styles.commentInput}
            editable={canEditWormData}
            onChangeText={onChangeComment}
            value={wormData[field.id]?.comment || ''}
            multiline
          />
        </>
      )}
    </View>
  )
}

export default TextField
