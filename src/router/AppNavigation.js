import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'

import * as View from '~/screens'
import {Screen} from '~/utils'

const Stack = createStackNavigator()

const AppNavigation = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName={Screen.DashboardScreen}>
      <Stack.Screen name={Screen.DashboardScreen} component={View.DashboardScreen} />
      <Stack.Screen
        name={Screen.WormScreen}
        component={View.WormScreen}
        options={{
          gestureEnabled: false
        }}
      />
    </Stack.Navigator>
  )
}

export default AppNavigation
