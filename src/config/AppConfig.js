import {Config} from 'react-native-config'

const AppConfig = {
  BASE_URL: Config.BASE_URL,
  STORAGE_ENCRYPTION_KEY: Config.STORAGE_ENCRYPTION_KEY
}

export default AppConfig
