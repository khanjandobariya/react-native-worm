import React from 'react'
import {LogBox, Text, TouchableOpacity} from 'react-native'
import Toast from 'react-native-toast-message'
import {NavigationContainer} from '@react-navigation/native'

import {AppLoader} from './components'
import {ThemeProvider, WormProvider} from './context'
import {AppNavigation} from './router'
import {navigationRef} from './router/RootNavigator'
import {Loader} from './utils'

import './i18n/i18n'

LogBox.ignoreAllLogs()
Text.defaultProps = Text.defaultProps || {}
Text.defaultProps.allowFontScaling = false
TouchableOpacity.defaultProps = TouchableOpacity.defaultProps || {}
TouchableOpacity.defaultProps.activeOpacity = 0.8

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <ThemeProvider>
        <WormProvider>
          <AppNavigation />
          <Toast />
          <AppLoader ref={(ref) => Loader.setLoader(ref)} />
        </WormProvider>
      </ThemeProvider>
    </NavigationContainer>
  )
}

export default App
