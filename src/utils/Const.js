const Const = {
  jwtToken: null,
  eventEmitter: {
    logout: 'logout'
  }
}

export default Const
