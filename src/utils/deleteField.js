const deleteField = (fields, targetField) => {
  for (let i = 0; i < fields.length; i += 1) {
    const field = fields[i]
    if (field.fields) {
      const bool = deleteField(field.fields, targetField)
      if (bool) {
        return bool
      }
    }
    if (field.id === targetField.id) {
      fields.splice(i, 1)
      return true
    }
  }
  return false
}

export default deleteField
