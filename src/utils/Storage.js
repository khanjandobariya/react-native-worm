import {MMKVLoader} from 'react-native-mmkv-storage'

import Const from './Const'

const storage = new MMKVLoader().withEncryption().initialize()

const Key = {
  token: 'token'
}

const setJwtToken = (payload) => {
  Const.jwtToken = payload
  storage.setString(Key.token, payload)
}

const getJwtToken = (callback) => {
  storage
    .getStringAsync(Key.token)
    .then((token) => {
      Const.jwtToken = token
      if (callback) {
        callback(token)
      }
    })
    .catch(() => {
      if (callback) {
        callback(null)
      }
    })
}

const clearAllData = () => {
  storage.removeItem(Key.token)
  Const.jwtToken = null
}

const Storage = {
  setJwtToken,
  getJwtToken,
  clearAllData
}

export default Storage
