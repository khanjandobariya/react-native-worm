const newChoiceValue = {
  id: null,
  wLabel: {
    label: {
      text: 'New Option Label',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  frozen: false
}

export {newChoiceValue}
