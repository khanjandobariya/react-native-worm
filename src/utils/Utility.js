import Toast from 'react-native-toast-message'

const successToast = (message) => {
  Toast.show({
    type: 'success',
    text1: message
  })
}

const errorToast = (message) => {
  Toast.show({
    type: 'error',
    text1: message
  })
}

const infoToast = (message) => {
  Toast.show({
    type: 'info',
    text1: message
  })
}

const validateEmail = (email) => {
  const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/
  return emailRegex.test(email)
}

export {errorToast, infoToast, successToast, validateEmail}
