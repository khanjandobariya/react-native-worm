import shortid from 'shortid'

const newYesNoMcssField = {
  id: shortid.generate(),
  wLabel: {
    label: {
      text: 'New Yes or No Question',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  desc: null,
  help: null,
  options: {
    values: [
      {
        id: shortid.generate(),
        wLabel: {
          label: {
            text: 'Yes',
            lKey: null,
            hidn: false,
            trs: {},
            html: false
          }
        },
        frozen: false
      },
      {
        id: shortid.generate(),
        wLabel: {
          label: {
            text: 'No',
            lKey: null,
            hidn: false,
            trs: {},
            html: false
          }
        },
        frozen: false
      }
    ],
    other: {
      enabled: false,
      frozen: false
    }
  },
  constraints: null,
  frozen: false,
  type: 'EN'
}

export {newYesNoMcssField}
