const updateField = (fields, updatedField) => {
  for (let i = 0; i < fields.length; i += 1) {
    const field = fields[i]
    if (field.fields) {
      const bool = updateField(field.fields, updatedField)
      if (bool) {
        return bool
      }
    }
    if (field.id === updatedField.id) {
      fields[i] = updatedField
      return true
    }
  }
  return false
}

export default updateField
