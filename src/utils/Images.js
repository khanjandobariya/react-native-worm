const Images = {
  illustration1: require('../assets/images/illustration1.png'),
  form: require('../assets/images/form.png'),
  notes: require('../assets/images/notes.png'),
  dots: require('../assets/images/dots.png'),
  empty: require('../assets/images/empty.png'),
  plus: require('../assets/images/plus.png'),
  menu: require('../assets/images/menu.png'),
  setting: require('../assets/images/setting.png'),
  close: require('../assets/images/close.png'),
  save: require('../assets/images/save.png'),
  share: require('../assets/images/share.png'),
  delete: require('../assets/images/delete.png'),
  calender: require('../assets/images/calender.png'),
  clip: require('../assets/images/clip.png'),
  msg: require('../assets/images/msg.png'),
  options: require('../assets/images/options.png'),
  multiple: require('../assets/images/multiple.png'),
  more: require('../assets/images/more.png'),
  pencil: require('../assets/images/pencil.png'),
  newcalender: require('../assets/images/newcalender.png'),
  bin: require('../assets/images/bin.png')
}

export default Images
