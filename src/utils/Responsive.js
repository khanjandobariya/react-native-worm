import {Dimensions, Platform, StatusBar} from 'react-native'
import {isTablet} from 'react-native-device-info'

const {height: W_HEIGHT, width: W_WIDTH} = Dimensions.get('window')

// eslint-disable-next-line import/no-mutable-exports
let isIPhoneX = false

if (Platform.OS === 'ios' && !Platform.isPad) {
  isIPhoneX =
    W_HEIGHT === 780 ||
    W_WIDTH === 780 ||
    W_HEIGHT === 812 ||
    W_WIDTH === 812 ||
    W_HEIGHT === 844 ||
    W_WIDTH === 844 ||
    W_HEIGHT === 896 ||
    W_WIDTH === 896 ||
    W_HEIGHT === 926 ||
    W_WIDTH === 926
}

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const widthPx = (widthPercent) => {
  return (screenWidth * widthPercent) / 100
}

const heightPx = (heightPercent) => {
  return ((screenHeight - getStatusBarHeight()) * heightPercent) / 100
}

const font = (size) => {
  return (screenWidth * size) / 100
}

const getStatusBarHeight = () => {
  const statusBarHeight = Platform.select({
    ios: isIPhoneX ? 78 : 20,
    android: StatusBar.currentHeight > 24 ? 0 : StatusBar.currentHeight,
    default: 0
  })
  return statusBarHeight
}

const isIPhoneXSeries = () => {
  if (Platform.OS === 'android') {
    return 0
  }
  return isIPhoneX ? 34 : 0
}

const isAndroidNouch = Platform.OS === 'android' ? StatusBar.currentHeight > 24 : false
const [shortDimension, longDimension] =
  W_WIDTH < W_HEIGHT ? [W_WIDTH, W_HEIGHT] : [W_HEIGHT, W_WIDTH]

// guideline size
const guidelineBaseWidth = 428
const guidelineBaseHeight = 926

const scale = (size) => (shortDimension / guidelineBaseWidth) * size

const verticalScale = (size) => (longDimension / guidelineBaseHeight) * size

const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor

const isTab = isTablet()

export {
  font,
  getStatusBarHeight,
  heightPx,
  isAndroidNouch,
  isIPhoneX,
  isIPhoneXSeries,
  isTab,
  moderateScale,
  scale,
  verticalScale,
  widthPx
}
