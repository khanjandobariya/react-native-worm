const newSection = {
  id: null,
  frozen: false,
  constraints: null,
  wLabel: {label: {text: 'New Section Title', lKey: null, hidn: false, trs: {}, html: false}},
  fields: [],
  type: 'SEC'
}
const newTextField = {
  id: null,
  wLabel: {
    label: {
      text: 'New Text Field Label',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  desc: null,
  help: null,
  phold: null,
  constraints: null,
  frozen: false,
  type: 'S'
}

const newSingleValueEnumField = {
  id: null,
  wLabel: {
    label: {
      text: 'New Multiple Choice Single Select Label',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  desc: null,
  help: null,
  options: {
    values: [],
    other: {
      enabled: false,
      frozen: false
    }
  },
  constraints: null,
  frozen: false,
  type: 'EN'
}

const newStaticTextField = {
  id: null,
  wLabel: {
    label: {
      text: 'New Static Text',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  desc: null,
  help: null,
  constraints: null,
  frozen: false,
  type: 'ST'
}
const newDateField = {
  id: null,
  wLabel: {
    label: {
      text: 'New Date Label',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    }
  },
  srcUrl: '',
  desc: null,
  help: null,
  constraints: null,
  frozen: false,
  type: 'DT'
}

export {newDateField, newSection, newSingleValueEnumField, newStaticTextField, newTextField}
