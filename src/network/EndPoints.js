const EndPoints = {
  login: 'u/login',
  logout: 'user/logout',
  profile: 'user/profile',
  myTasksCount: 'my_tasks/counts',
  myTasks: 'my_tasks',
  userProfileImg: 'user/profile/photo',
  updateUserProfile: 'user/profile/x_full_name',
  eSign: 'user/profile/esig',
  eRegModules: 'v1/app/ereg/modules',
  eRegStudySites: 'v1/app/ereg/modules/mo::study_sites',
  eRegSiteLogo:
    'v1/app/ereg/modules/mo::study_sites/mo::commons/mo::sponsor_logo/mi::SITE_LOGO_ID/binary',
  eRegDashboard: 'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::dashboard',
  eRegSideMenuList: 'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID',
  eRegBinderDocList:
    'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::documents',
  eRegBinderDocTypes:
    'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::doc-types',
  eRegBinderFileDownload:
    'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::documents/mi::DOC_ID/mo::doc-versions/mi::DOC_VERSION_ID/binary',
  eRegDocVersions:
    'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::documents/mi::DOC_ID/mo::doc-versions',
  eRegDocFolders: 'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::folders',
  eRegAuditTrail:
    'v1/app/ereg/modules/mo::study_sites/mi::STUDY_SITE_ID/mo::binder/mo::documents/mi::DOC_ID/mo::doc-trail'
}

export default EndPoints
