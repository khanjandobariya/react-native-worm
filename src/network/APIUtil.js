/* eslint-disable no-console */
import {CommonActions} from '@react-navigation/native'
import Axios from 'axios'

import {AppConfig} from '~/config'
import {navigationRef} from '~/router/RootNavigator'
import {Const, Screen, Storage} from '~/utils'

const axiosInstance = Axios.create({
  baseURL: AppConfig.BASE_URL
})

axiosInstance.interceptors.request.use(
  (request) => {
    if (Const.jwtToken) {
      request.headers.Authorization = Const.jwtToken
    }
    console.log(`axios request => ${request.url}`, request)
    return request
  },
  (error) => {
    console.log('axios request error =>', error)
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  (response) => {
    console.log(`axios response =>${response?.config?.url}`, response)
    return response
  },
  (error) => {
    console.log('axios response error =>', error.response || error)
    return Promise.reject(error)
  }
)

const getFormData = (object) => {
  const formData = new FormData()
  Object.keys(object).forEach((key) => formData.append(key, object[key]))
  return formData
}

const APICall = async (method, url, payload, headers, formData) => {
  method = method.toLowerCase()
  const config = {
    method,
    url,
    timeout: 1000 * 60 * 2
  }
  if (url) {
    config.url = url
  }
  if (payload && method === 'get') {
    config.params = payload
  } else if (formData) {
    config.data = getFormData(payload)
    config.headers = {
      ...config.headers,
      'Content-Type': 'multipart/form-data'
    }
  } else if (payload) {
    config.data = payload
  }
  if (headers) {
    config.headers = {...config.headers, ...headers}
  }

  return new Promise((resolve, reject) => {
    axiosInstance(config)
      .then((res) => {
        if (res.data.status === 'PASS') {
          resolve(res.data.data)
        } else if (res.data?.reason === 'UNAUTHENTICATED') {
          // redirect to login screen
          navigationRef.current.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: Screen.LoginScreen
                }
              ]
            })
          )
          Storage.clearAllData()
          reject(res.data)
        } else {
          reject(res.data)
        }
      })
      .catch((error) => {
        if (error.code === 'ECONNABORTED') {
          reject({reason: 'Request timeout. Please check your internet connection'})
          return
        }
        reject({reason: 'Something went wrong, Please try again later.'})
      })
  })
}

// GET API request
const getAndSetModuleData = (
  url,
  payload,
  successCallback,
  errorCallback,
  finallyCallback,
  headers,
  formData
) => {
  APICall('GET', url, payload, headers, formData)
    .then((response) => {
      if (successCallback) {
        successCallback(response)
      }
    })
    .catch((error) => {
      if (errorCallback) {
        errorCallback(error)
      }
    })
    .finally(() => {
      if (finallyCallback) {
        finallyCallback()
      }
    })
}

// POST API request
const postAndSetModuleData = (
  url,
  payload,
  successCallback,
  errorCallback,
  finallyCallback,
  headers,
  formData
) => {
  APICall('POST', url, payload, headers, formData)
    .then((response) => {
      if (successCallback) {
        successCallback(response)
      }
    })
    .catch((error) => {
      if (errorCallback) {
        errorCallback(error)
      }
    })
    .finally(() => {
      if (finallyCallback) {
        finallyCallback()
      }
    })
}

// PUT API request
const putAndSetModuleData = (
  url,
  payload,
  successCallback,
  errorCallback,
  finallyCallback,
  headers,
  formData
) => {
  APICall('PUT', url, payload, headers, formData)
    .then((response) => {
      if (successCallback) {
        successCallback(response)
      }
    })
    .catch((error) => {
      if (errorCallback) {
        errorCallback(error)
      }
    })
    .finally(() => {
      if (finallyCallback) {
        finallyCallback()
      }
    })
}

// DELETE API request
const deletAndSetModuleData = (
  url,
  payload,
  successCallback,
  errorCallback,
  finallyCallback,
  headers,
  formData
) => {
  APICall('DELETE', url, payload, headers, formData)
    .then((response) => {
      if (successCallback) {
        successCallback(response)
      }
    })
    .catch((error) => {
      if (errorCallback) {
        errorCallback(error)
      }
    })
    .finally(() => {
      if (finallyCallback) {
        finallyCallback()
      }
    })
}

export {deletAndSetModuleData, getAndSetModuleData, postAndSetModuleData, putAndSetModuleData}
