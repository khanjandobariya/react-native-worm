import {
  deletAndSetModuleData,
  getAndSetModuleData,
  postAndSetModuleData,
  putAndSetModuleData
} from './APIUtil'
import EndPoints from './EndPoints'

export {
  deletAndSetModuleData,
  EndPoints,
  getAndSetModuleData,
  postAndSetModuleData,
  putAndSetModuleData
}
