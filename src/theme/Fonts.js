export const Fonts = {
  bold: 'Gilroy-Bold',
  semi_bold: 'Gilroy-SemiBold',
  regular: 'Gilroy-Regular',
  medium: 'Gilroy-Medium',
  other: 'GreatVibes-Regular'
}
