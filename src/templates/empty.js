import shortid from 'shortid'

import {Images} from '~/utils'

export const empty = {
  id: shortid.generate(),
  type: 'W',
  frozen: false,
  constraints: null,
  ui: {
    style: {
      backgroundColor: null
    }
  },
  wLabel: {
    label: {
      text: 'Blank Form',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    },
    ui: {style: {}, imgSrc: Images.notes}
  },
  desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  fields: []
}

export const pre_study_eval = {
  id: shortid.generate(),
  type: 'W',
  frozen: false,
  constraints: null,
  ui: {
    style: {
      backgroundColor: null
    }
  },
  wLabel: {
    label: {
      text: 'Pre-Study Visit Evaluation Checklist',
      lKey: null,
      hidn: false,
      trs: {},
      html: false
    },
    ui: {style: {}, imgSrc: Images.form}
  },
  desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  fields: [
    {
      id: shortid.generate(),
      frozen: false,
      constraints: null,
      wLabel: {
        label: {
          text: 'Site Identification ',
          lKey: null,
          hidn: false,
          trs: {},
          html: false
        }
      },
      fields: [
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Investigator:',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          phold: null,
          constraints: null,
          frozen: false,
          type: 'S'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Sub-Investigator(s): ',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: '*note if any FDA inspections and if 483 was issued',
          help: null,
          phold: null,
          constraints: null,
          frozen: false,
          type: 'S',
          commentsEnabled: true
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Primary Study Coordinator:',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: 'Back up Study Coordinator:',
          help: null,
          phold: null,
          constraints: null,
          frozen: false,
          type: 'S',
          commentsEnabled: true
        }
      ],
      type: 'SEC'
    },
    {
      id: shortid.generate(),
      frozen: false,
      constraints: null,
      wLabel: {
        label: {
          text: 'Protocol and Compliance',
          lKey: null,
          hidn: false,
          trs: {},
          html: false
        }
      },
      fields: [
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Do the investigator and site personnel understand:',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          constraints: null,
          frozen: false,
          type: 'ST'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Safety and efficacy data and study endpoints',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Inclusion/exclusion criteria?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Study procedures (visit schedule, required tests, Pulmonary  Function Testing, Neurology Exam by Neurologist, etc)?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Concomitant medications and disallowed medications/therapies/procedures?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        }
      ],
      type: 'SEC'
    },
    {
      id: shortid.generate(),
      frozen: false,
      constraints: null,
      wLabel: {
        label: {
          text: 'Subject Population and Recruitment',
          lKey: null,
          hidn: false,
          trs: {},
          html: false
        }
      },
      fields: [
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Were protocol-required subject enrollment timelines and patient expectations reviewed?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Can the investigator meet protocol-required subject enrollment expectations and complete the trial within the timelines?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: null,
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'How many subjects (with the above indication) does the investigator see each month?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: '(5-15)',
          help: null,
          phold: '5-15',
          constraints: null,
          frozen: false,
          type: 'S'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'How many of these subjects would preliminarily be eligible?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: '(2-10)',
          help: null,
          phold: '2-10',
          constraints: null,
          frozen: false,
          type: 'S'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'How many patients can they enroll in the study per month? (2-4) and/or per 6 months (12-16)',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: '(2-4) and/or per 6 months (12-16)',
          help: null,
          phold: '2-4 or 12-16',
          constraints: null,
          frozen: false,
          type: 'S'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'How will the site identify patients and can they prescreen?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: ' (database, advertisement, referrals, community physicians/clinics-give examples of recruitment strategies in text)',
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN'
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Competing Studies',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: 'If yes, indicate stage of study, recruitment timeline and effect on this study and plans for a fair distribution of subjects when competing with this study for the same patients.',
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN',
          commentsEnabled: true
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'Are there any foreseeable obstacles to enrollment?',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          desc: 'If yes, explain.',
          help: null,
          options: {
            values: [
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'Yes',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              },
              {
                id: shortid.generate(),
                wLabel: {
                  label: {
                    text: 'No',
                    lKey: null,
                    hidn: false,
                    trs: {},
                    html: false
                  }
                },
                frozen: false
              }
            ],
            other: {
              enabled: false,
              frozen: false
            }
          },
          constraints: null,
          frozen: false,
          type: 'EN',
          commentsEnabled: true
        },
        {
          id: shortid.generate(),
          wLabel: {
            label: {
              text: 'When is first patient enrolled planned? ',
              lKey: null,
              hidn: false,
              trs: {},
              html: false
            }
          },
          srcUrl: '',
          desc: null,
          help: null,
          constraints: null,
          frozen: false,
          type: 'DT',
          phold: 'Select Date',
          commentsEnabled: true
        }
      ],
      type: 'SEC'
    }
  ]
}
