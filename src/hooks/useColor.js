import {useContext} from 'react'

import {ThemeContext} from '~/context'
import {Theme} from '~/theme'

const useColor = () => {
  const context = useContext(ThemeContext)
  const selectedTheme = context.theme === 0 ? 'DefaultTheme' : 'DarkTheme'
  const color = Theme[selectedTheme]
  return color
}

export default useColor
