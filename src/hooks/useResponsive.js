import {useEffect, useState} from 'react'
import {Platform, StatusBar, useWindowDimensions} from 'react-native'

const useResponsive = () => {
  const [windowHeight, setHeight] = useState(0)
  const [windowWidth, setWidth] = useState(0)

  let isIPhoneX = false
  const {height: W_HEIGHT, width: W_WIDTH} = useWindowDimensions()
  if (Platform.OS === 'ios' && !Platform.isPad) {
    isIPhoneX =
      W_HEIGHT === 780 ||
      W_WIDTH === 780 ||
      W_HEIGHT === 812 ||
      W_WIDTH === 812 ||
      W_HEIGHT === 844 ||
      W_WIDTH === 844 ||
      W_HEIGHT === 896 ||
      W_WIDTH === 896 ||
      W_HEIGHT === 926 ||
      W_WIDTH === 926
  }

  const {height, width} = useWindowDimensions()
  const [shortDimension, longDimension] = width < height ? [width, height] : [height, width]

  // guideline size
  const guidelineBaseWidth = 428
  const guidelineBaseHeight = 926

  const scale = (size) => (shortDimension / guidelineBaseWidth) * size

  const verticalScale = (size) => (longDimension / guidelineBaseHeight) * size

  const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor

  const getStatusBarHeight = () => {
    return Platform.select({
      ios: isIPhoneX ? 78 : 20,
      android: StatusBar.currentHeight > 24 ? 0 : StatusBar.currentHeight,
      default: 0
    })
  }
  const widthPx = (widthPercent) => {
    const elemWidth = typeof widthPercent === 'number' ? widthPercent : parseFloat(widthPercent)
    return (width * elemWidth) / 100
  }

  const heightPx = (heightPercent) => {
    const elemHeight = typeof heightPercent === 'number' ? heightPercent : parseFloat(heightPercent)
    return ((height - getStatusBarHeight().toFixed(0)) * elemHeight) / 100
  }

  useEffect(() => {
    setWidth(height)
    setHeight(width)
  }, [height, width])

  return {
    windowWidth,
    windowHeight,
    scale,
    verticalScale,
    moderateScale,
    widthPx,
    heightPx,
    isIPhoneX
  }
}
export default useResponsive
