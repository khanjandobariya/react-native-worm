import React from 'react'
import {Platform} from 'react-native'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'

const AppScrollView = (props) => {
  const {children} = props
  return (
    <KeyboardAwareScrollView
      {...props}
      keyboardShouldPersistTaps={'handled'}
      extraHeight={Platform.OS === 'ios' ? 120 : 30}
      enableOnAndroid
    >
      {children}
    </KeyboardAwareScrollView>
  )
}

export default AppScrollView
