import {StyleSheet} from 'react-native'

import {useColor} from '~/hooks'

export const useStyle = () => {
  const colors = useColor()

  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.white
    },
    mainContainer: {
      flex: 1
    },
    topBar: {
      backgroundColor: colors.white
    }
  })
}
