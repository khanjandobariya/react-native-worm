import React from 'react'
import {SafeAreaView, StatusBar, View} from 'react-native'

import {useColor} from '~/hooks'
import {useStyle} from './AppContainer.styles'

const AppContainer = (props) => {
  const colors = useColor()
  const {
    isTopSafeArea,
    isBottomSafeArea,
    topColor = colors.white,
    bottomColor = colors.greyShadDD,
    barStyle,
    children
  } = props
  const TopComponent = isTopSafeArea ? SafeAreaView : View
  const BottomComponent = isBottomSafeArea ? SafeAreaView : View
  const styles = useStyle()

  return (
    <View style={styles.container}>
      <TopComponent style={[styles.topBar, {backgroundColor: topColor}]} />
      <StatusBar barStyle={barStyle} backgroundColor={topColor} />
      <View style={styles.mainContainer}>{children}</View>
      <BottomComponent style={{backgroundColor: bottomColor}} />
    </View>
  )
}

export default AppContainer

AppContainer.defaultProps = {
  isTopSafeArea: true,
  isBottomSafeArea: false,
  barStyle: 'dark-content'
}
