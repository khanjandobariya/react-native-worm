import {StyleSheet} from 'react-native'

import {useColor} from '~/hooks'
import {Fonts} from '~/theme'
import {moderateScale, scale, verticalScale} from '~/utils/Responsive'

export const useStyle = () => {
  const colors = useColor()

  return StyleSheet.create({
    container: {
      margin: 0,
      flex: 1,
      marginHorizontal: scale(20)
    },
    contentContainer: {
      width: '100%',
      backgroundColor: colors.white,
      paddingHorizontal: scale(20),
      paddingVertical: verticalScale(15),
      borderRadius: moderateScale(8)
    },
    rowView: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    title: {
      fontSize: scale(18),
      fontFamily: Fonts.semi_bold,
      color: colors.black
    },
    closeBtn: {
      width: scale(20),
      height: scale(20),
      aspectRatio: 1,
      justifyContent: 'center',
      alignItems: 'flex-end'
    },
    closeBtnIcon: {
      width: '100%',
      height: '100%',
      tintColor: colors.secondary
    },
    msgText: {
      fontSize: scale(16),
      fontFamily: Fonts.medium,
      color: colors.black,
      marginTop: verticalScale(15)
    },
    btnRow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: verticalScale(20)
    },
    calcelBtn: {
      height: verticalScale(35),
      paddingHorizontal: scale(15),
      paddingVertical: scale(5)
    },
    confirmBtn: {
      height: verticalScale(35),
      backgroundColor: colors.primary,
      paddingHorizontal: scale(15),
      paddingVertical: scale(5),
      borderWidth: 0
    },
    btnText: {
      color: colors.white
    }
  })
}
