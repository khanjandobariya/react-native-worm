import React from 'react'
import {useTranslation} from 'react-i18next'
import {Image, Text, TouchableOpacity, View} from 'react-native'
import Modal from 'react-native-modal'

import {Images} from '~/utils'
import AppButton from '../button/AppButton'
import {useStyle} from './ConfirmationModal.styles'

const ConfirmationModal = ({isVisible, onClose, onPressConfirm}) => {
  const styles = useStyle()
  const {t} = useTranslation()

  return (
    <Modal
      isVisible={isVisible}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={styles.container}
      onBackdropPress={onClose}
    >
      <View style={styles.contentContainer}>
        <View style={styles.rowView}>
          <Text style={styles.title}>{t('WM4')}</Text>
          <TouchableOpacity style={styles.closeBtn} onPress={onClose}>
            <Image source={Images.close} resizeMode={'contain'} style={styles.closeBtnIcon} />
          </TouchableOpacity>
        </View>
        <Text style={styles.msgText}>{t('WM5')}</Text>
        <View style={styles.btnRow}>
          <AppButton containerStyle={styles.calcelBtn} label={t('WM6')} onPress={onClose} />
          <AppButton
            containerStyle={styles.confirmBtn}
            textStyle={styles.btnText}
            label={t('WM7')}
            onPress={onPressConfirm}
          />
        </View>
      </View>
    </Modal>
  )
}

export default ConfirmationModal
