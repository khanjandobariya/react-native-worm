import React from 'react'
import {Image, Text, TouchableOpacity} from 'react-native'

import {useStyle} from './AppButton.styles'

const AppButton = (props) => {
  const {label, containerStyle, textStyle, onPress, icon} = props
  const styles = useStyle()

  return (
    <TouchableOpacity style={[styles.container, containerStyle]} onPress={() => onPress()}>
      {icon && <Image resizeMode={'contain'} source={icon} style={styles.icon} />}
      <Text style={[styles.btnText, textStyle]}>{label}</Text>
    </TouchableOpacity>
  )
}

AppButton.defaultProps = {
  label: '',
  containerStyle: {},
  textStyle: {},
  onPress: () => {},
  icon: null
}

export default AppButton
