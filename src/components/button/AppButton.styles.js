import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      height: verticalScale(45),
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: moderateScale(8),
      borderWidth: 1,
      borderColor: colors.greyShadD9,
      paddingHorizontal: scale(15),
      paddingVertical: scale(5),
      backgroundColor: colors.white
    },
    icon: {
      width: scale(16),
      height: scale(16),
      marginRight: scale(10)
    },
    btnText: {
      fontFamily: Fonts.regular,
      fontSize: moderateScale(14),
      color: colors.black
    }
  })
}
