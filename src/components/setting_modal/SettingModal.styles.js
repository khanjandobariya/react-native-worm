import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Fonts} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale, verticalScale} = useResponsive()

  return StyleSheet.create({
    container: {
      flex: 1,
      margin: 0,
      padding: 0,
      justifyContent: 'flex-end'
    },
    contentContainer: {
      backgroundColor: colors.white,
      padding: scale(10),
      borderTopRightRadius: moderateScale(25),
      borderTopLeftRadius: moderateScale(25),
      paddingVertical: verticalScale(30)
    },
    itemContainer: {
      flexDirection: 'row',
      paddingHorizontal: scale(10),
      alignItems: 'center'
    },
    itemImg: {
      width: scale(22),
      height: scale(22),
      tintColor: colors.darkGreyText
    },
    itemText: {
      fontFamily: Fonts.medium,
      fontSize: moderateScale(18),
      color: colors.darkGreyText,
      marginLeft: scale(20)
    },
    activeImg: {
      tintColor: colors.primary
    },
    activeText: {
      color: colors.primary
    },
    separator: {
      width: '100%',
      height: scale(1),
      backgroundColor: colors.lightBorder,
      marginVertical: verticalScale(15)
    }
  })
}
