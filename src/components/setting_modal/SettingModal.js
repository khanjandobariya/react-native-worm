import React, {useContext, useState} from 'react'
import {useTranslation} from 'react-i18next'
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native'
import Modal from 'react-native-modal'

import {WormContext} from '~/context'
import {Images} from '~/utils'
import {useStyle} from './SettingModal.styles'

const SettingModal = ({isVisible, onRequestClose}) => {
  const {t} = useTranslation()
  const styles = useStyle()
  const OPTION_LIST = [
    {
      key: 'EDIT_SCHEMA',
      icon: Images.pencil,
      title: t('WM26')
    },
    {
      key: 'EDIT_DATA',
      icon: Images.pencil,
      title: t('WM27')
    }
  ]

  const [activeIndex, setActiveIndex] = useState(null)
  const {canEditWormSchema, setCanEditWormSchema, canEditWormData, setCanEditWormData} =
    useContext(WormContext)

  const onPressItem = (key, index) => {
    if (key === 'EDIT_SCHEMA') {
      if (canEditWormSchema) {
        setCanEditWormSchema(false)
        setCanEditWormData(true)
        setActiveIndex(null)
      } else {
        setCanEditWormSchema(true)
        setCanEditWormData(false)
        setActiveIndex(index)
      }
    } else if (key === 'EDIT_DATA') {
      if (canEditWormData) {
        setCanEditWormData(false)
        setActiveIndex(null)
      } else {
        setCanEditWormData(true)
        setCanEditWormSchema(false)
        setActiveIndex(index)
      }
    }
    onRequestClose()
  }

  const renderItem = ({item, index}) => {
    const isActive = activeIndex === index
    return (
      <TouchableOpacity style={styles.itemContainer} onPress={() => onPressItem(item.key, index)}>
        <Image
          source={Images.pencil}
          resizeMode={'contain'}
          style={[styles.itemImg, isActive && styles.activeImg]}
        />
        <Text style={[styles.itemText, isActive && styles.activeText]}>{item.title}</Text>
      </TouchableOpacity>
    )
  }

  const renderItemSeparator = () => <View style={styles.separator} />

  return (
    <Modal
      animationType={'slide'}
      isVisible={isVisible}
      avoidKeyboard
      onRequestClose={onRequestClose}
      style={styles.container}
      onBackButtonPress={onRequestClose}
      onBackdropPress={onRequestClose}
    >
      <View style={styles.contentContainer}>
        <FlatList
          data={OPTION_LIST}
          keyExtractor={(_, i) => `menu-${i}`}
          renderItem={renderItem}
          ItemSeparatorComponent={renderItemSeparator}
        />
      </View>
    </Modal>
  )
}

export default SettingModal
