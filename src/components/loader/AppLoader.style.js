import {StyleSheet} from 'react-native'

import {useColor, useResponsive} from '~/hooks'
import {Opacity} from '~/theme'

export const useStyle = () => {
  const colors = useColor()
  const {scale, moderateScale} = useResponsive()

  return StyleSheet.create({
    container: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: `${colors.modalOverlay}${Opacity[50]}`
    },
    innerContainer: {
      width: scale(70),
      height: scale(70),
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors.background,
      borderRadius: moderateScale(10),
      shadowColor: colors.background,
      shadowOffset: {
        width: 0,
        height: 3
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,
      elevation: 7
    }
  })
}
