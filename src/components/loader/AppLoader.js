import React, {forwardRef, useImperativeHandle, useState} from 'react'
import {ActivityIndicator, View} from 'react-native'

import {useColor} from '~/hooks'
import {useStyle} from './AppLoader.style'

const AppLoader = forwardRef((props, ref) => {
  const [isLoading, setLoading] = useState(false)
  const colors = useColor()
  const styles = useStyle()

  useImperativeHandle(
    ref,
    () => ({
      showLoader
    }),
    []
  )

  const showLoader = (isLoading) => {
    setLoading(isLoading)
  }

  if (isLoading) {
    return (
      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <ActivityIndicator color={colors.primary} size={'large'} />
        </View>
      </View>
    )
  }
  return null
})

export default AppLoader
