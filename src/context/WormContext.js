import React, {createContext, useCallback, useMemo, useState} from 'react'
import shortid from 'shortid'

import deleteField from '~/utils/deleteField'
import {newSection} from '~/utils/new-field'
import updateField from '~/utils/updateField'

const WormContext = createContext({})

const WormProvider = ({children}) => {
  const [canEditWormSchema, setCanEditWormSchema] = useState(false)
  const [canEditWormData, setCanEditWormData] = useState(true)

  const [wormData, setWormData] = useState({})
  const [wormSchema, setWormSchema] = useState(null)
  const [selectedField, setSelectedField] = useState(null)

  const onWormSchemaChange = useCallback(
    (updatedWormFields) => {
      if (updatedWormFields?.id === wormSchema?.id) {
        setWormSchema(updatedWormFields)
      } else {
        setWormSchema({...wormSchema, fields: updatedWormFields})
      }
    },
    [wormSchema, setWormSchema]
  )

  const onChangeWormData = useCallback(
    (updatedWormData) => {
      setWormData({...wormData, ...updatedWormData})
    },
    [setWormData, wormData]
  )

  const onFieldUpdate = useCallback(
    (updatedField, updateSelectedField = true) => {
      if (updatedField && updatedField?.id === wormSchema?.id) {
        onWormSchemaChange(updatedField)
        if (updateSelectedField) {
          setSelectedField(updatedField)
        }
      } else {
        const updatedWormFields = JSON.parse(JSON.stringify(wormSchema.fields))
        const bool = updateField(updatedWormFields, updatedField)
        if (bool) {
          onWormSchemaChange(updatedWormFields)
          if (updateSelectedField) {
            setSelectedField(updatedField)
          }
        }
      }
    },
    [wormSchema, onWormSchemaChange, setSelectedField, updateField]
  )

  const onFieldDelete = useCallback(
    (field) => {
      const updatedWormFields = JSON.parse(JSON.stringify(wormSchema.fields))
      const bool = deleteField(updatedWormFields, field)
      if (bool) {
        onWormSchemaChange(updatedWormFields)
        setSelectedField(null)
      }
    },
    [wormSchema, onWormSchemaChange, setSelectedField, deleteField]
  )

  const onNewSection = useCallback(() => {
    const newField = {...newSection}
    newField.id = shortid.generate()
    const updatedWormFields = JSON.parse(JSON.stringify(wormSchema.fields))
    updatedWormFields.push(newField)
    onWormSchemaChange(updatedWormFields)
    setSelectedField(newField)
  }, [wormSchema, onWormSchemaChange, setSelectedField])

  const value = useMemo(() => {
    return {
      canEditWormSchema,
      canEditWormData,
      wormData,
      wormSchema,
      selectedField,
      setCanEditWormSchema,
      setCanEditWormData,
      setWormData,
      setWormSchema,
      onWormSchemaChange,
      setSelectedField,
      onNewSection,
      onFieldUpdate,
      onFieldDelete,
      onChangeWormData
    }
  }, [
    canEditWormSchema,
    canEditWormData,
    wormData,
    wormSchema,
    selectedField,
    setCanEditWormSchema,
    setCanEditWormData,
    setWormData,
    setWormSchema,
    onWormSchemaChange,
    setSelectedField,
    onNewSection,
    onFieldUpdate,
    onFieldDelete,
    onChangeWormData
  ])

  return <WormContext.Provider value={value}>{children}</WormContext.Provider>
}

export {WormContext, WormProvider}
